@extends('layouts.app')

@section('content')

    <h2>Add user details</h2>

    <form method="post" action="/update" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-group row">
        <label for="titleid" class="col-sm-3 col-form-label">Name</label>
        <div class="col-sm-9">
            <input name="name" type="text" class="form-control" id="name" placeholder="{{ $name }}">
        </div>
    </div>

    <div class="form-group row">
        <label for="dob" class="col-sm-3 col-form-label">DOB</label>
        <div class="col-sm-9">
            <input name="DOB" type="text" class="form-control" id="DOB"
                   placeholder="DOB">
        </div>
    </div>
    <div class="form-group row">
        <div class="offset-sm-3 col-sm-9">
            <button type="submit" class="btn btn-primary">Submit user details</button>
        </div>
    </div>

        <div class="form-group row">
            <div class="offset-sm-3 col-sm-9">

                <a href="{{'/'}}" >
                    <button type="button" class="label label-default pull-xs-right">Back to Menu</button
                    ></a>

            </div>
        </div>
</form>

@endsection