<!-- Stored in resources/views/layouts/app.blade.php -->

{{--<html>--}}
{{--<head>--}}
{{--    <title>App Name - @yield('title')</title>--}}
{{--</head>--}}
{{--<body>--}}
{{--@section('sidebar')--}}
{{--    Pay Link Menu--}}
{{--@show--}}

{{--<div class="container">--}}
{{--    @yield('content')--}}
{{--</div>--}}
{{--</body>--}}
{{--</html>--}}


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Styles -->
    <link href="../../sass/app.css" rel="stylesheet">

    <title>App Name - @yield('title')</title>
</head>
<body>

@section('sidebar')
    Pay Link Menu
@show

<div class="container">
    @yield('content')
</div>


<div id="app">
    @include('flash-message')


</div>


<!-- Scripts -->
<script src="../../js/app.js"></script>
</body>
</html>