<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::redirect('/', '/menu');

Route::get('menu', function () {
    return view('child');
})->name('menu');


// FizzBuzz Route
Route::get('/FizzBuzz', 'PaylinkController@FizzBuzz')->name('FizzBuzz');

//gettersAndSetters route

Route::get('/showForm/{id?}', 'MagicGetterSetter@showForm')->name('form');

//gettersAndSetters route
Route::get('/getUserName/{id?}', 'MagicGetterSetter@getUserName')->name('getUserName');

Route::post('/update', 'MagicGetterSetter@setUserName');