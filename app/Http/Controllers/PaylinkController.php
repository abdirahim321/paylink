<?php


namespace App\Http\Controllers;


class PaylinkController extends Controller
{


    public $output = null;


    public function FizzBuzz()
    {
        for ($i = 1; $i <= 20; $i++) {

            // divisible by 3, print 'Fizz'
            if( $i % 3 == 0) {

              $this->output = "Fizz";

                //divisible by 3 & 5, 'FizzBuzz'
                if( $i % 3 == 0 && $i % 5 == 0) {

                      $this->output = 'FizzBuzz';

                }
            }

            //divisible by 5, print 'Buzz'
            elseif( $i % 5 == 0) {

                $this->output = 'Buzz';
               // echo $this->output;
            }


            // otherwise print the number
            else{
                $this->output =  $i;

            }

            echo $this->output.'</br>';
        }


    }

}