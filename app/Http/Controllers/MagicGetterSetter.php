<?php


namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class MagicGetterSetter
{


    public function getUserName(Request $request){

        if($request->route('id')){

            $value = $request->route('id');

            $user = new User();

           return($user->getName($value));

        }

    }


    public function showForm(){

        $user = User::find(1);

        if (!$user) {
                throw new ModelNotFoundException('User not found by ID ' . 1);
            }


        return view('create', ['name' => $user->name, 'id' => $user->id]);

    }


    public function setUserName(Request $request){


        if ($request->input('name')) {

                $value = $request->input('name');

                $user = new User();

               if($user->setName($value)){

                   return back()->with('success', 'Item created successfully!');

               }
        }
    }

}